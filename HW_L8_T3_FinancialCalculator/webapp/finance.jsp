<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>


<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="box1">
        <form method="POST" action="/lecture8/finance">
            <h1>Калькулятор доходности вклада</h1>
        <table>
            <tr>
                <td>Сумма на момент открытия</td>
                <td><input type="text" name="sum"></td>
            </tr>
            <tr>
                <td>Процентная ставка</td>
                <td><input type="text" name="percentage"></td>
            </tr>
            <tr>
                <td>Количество лет</td>
                <td><input type="text" name="years"></td>
            </tr>
        </table>
    <button id="button">Посчитать</button>
        </form>
    </div>
</body>
</html>