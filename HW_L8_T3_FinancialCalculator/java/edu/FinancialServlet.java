package edu;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/finance/*")
public class FinancialServlet extends HttpServlet {

    private String sum;
    private String percentage;
    private String years;
    private long result;
    private String flag = "get";
    private String resultMessage = "hhhhhh";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        sum = req.getParameter("sum");
        percentage = req.getParameter("percentage");
        years = req.getParameter("years");

        if (flag.equals("get")) {
            getServletContext().getRequestDispatcher("/finance.jsp").forward(req, resp);
        }else if (flag.equals("error")){
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
            flag = "get";
        }else if (flag.equals("result")){
            req.setAttribute("resultMessage", resultMessage);
            getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
            flag = "get";
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            double sumDigit = Double.parseDouble(req.getParameter("sum"));
            double percentageDigit = Double.parseDouble(req.getParameter("percentage"));
            double yearsDigit = Double.parseDouble(req.getParameter("years"));

            if (sumDigit < 50000){
                flag = "error";
            }else {
                flag = "result";
                result = Math.round((sumDigit / 100 * percentageDigit) * yearsDigit + sumDigit);
                resultMessage = "Итоговая сумма " + result + " рублей";
            }
        }catch(Exception ex){
            flag = "result";
            resultMessage = "Неверный формат данных.\nСкорректируйте значения";
        }
        resp.sendRedirect("finance");
    }
}