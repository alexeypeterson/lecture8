package edu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/author/*")
public class GreetingServlet extends HttpServlet {

    private String secName = "Петерсон";
    private String firstName = "Алексей";
    private String lastName = "Дмитриевич";
    private String phone = "+7 999 564 04 31";
    private String hobbies = "Настольные и компьютерные игры, музыка, книги";
    private String bitbucketURL = "https://bitbucket.org/alexeypeterson";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("surName", secName);
        req.setAttribute("firstName", firstName);
        req.setAttribute("lastName", lastName);
        req.setAttribute("phone", phone);
        req.setAttribute("hobbies", hobbies);
        req.setAttribute("bitbucketURL", bitbucketURL);

        getServletContext().getRequestDispatcher("/author.jsp").forward(req, resp);

    }
}
